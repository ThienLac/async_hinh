package com.async;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {
    ImageView hinh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hinh = (ImageView) findViewById(R.id.imageViewLoGo);
runOnUiThread(new Runnable() {
    @Override
    public void run() {
        new loadhinh().execute("http://hinhanhdep.pro/content/uploads/2014/09/hinh-anh-hoa-sen-dep-049-1.jpg");
    }
});
    }

    private class loadhinh extends AsyncTask<String, ImageView, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL u = new URL(params[0]);
                Bitmap bmp = BitmapFactory.decodeStream(u.openConnection().getInputStream());
                hinh.setImageBitmap(bmp);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
